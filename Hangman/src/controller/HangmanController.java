package controller;

import apptemplate.AppTemplate;
import data.GameData;
import data.GameTimer;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 * @author Tianyi Lan
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate      appTemplate; // shared reference to the application
    private GameData         gamedata;    // shared reference to the game being played, loaded or saved
    private GameState        gamestate;   // the state of the game being shown in the workspace
    private Text[]           progress;    // reference to the text area for the word
    private WordBox          boxes;       // reference to the boxes for the word
    private WordBox          letterBox;   // reference to the boxes for all English letters (A-Z)
    private boolean          success;     // whether or not player was successful
    private int              discovered;  // the number of letters already discovered
    private Button           gameButton;  // shared reference to the "start game" button
    private Button           gameHint;    // shared reference to the "hint" button
    private Button           gameInfo;    // shared reference to the "info" button
    private Label            remains;     // dynamically updated label that indicates the number of remaining guesses
    private Label            timer;       // dynamically updated label that reflects the elapsed time
    private ArrayList<Node>  shapes;      // dynamically render hangman graphics
    private GameTimer        gameTimer;   // game timer
    private Path             workFile;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void enableHintButton() {
        if (gameHint == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameHint = workspace.getHint();
        }
        gameHint.setDisable(false);
    }

    public void disableHintButton() {
        if (gameHint == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameHint = workspace.getHint();
        }
        gameHint.setDisable(true);
    }

    public void enableInfoButton() {
        if (gameInfo == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameInfo = workspace.getInfo();
        }
        gameInfo.setDisable(false);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {
        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        discovered = 0;
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        gamedata.init();
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);

        //show hangman pane
        HBox.setHgrow(gameWorkspace.getFigurePane(), Priority.ALWAYS);
        //make GameTextsPane look nicer
        gameWorkspace.getGameTextsPane().setSpacing(2);
        gameWorkspace.getGameTextsPane().setPadding(new Insets(2,2,2,2));
        //make guessed letter pane look nicer
        guessedLetters.setSpacing(1);
        //make tried letter pane look nicer
        showGridLetters(gameWorkspace.getTriedLetters());

        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remains.setFont(new Font("Arial Black", 16));
        Label remainingGuesses = new Label("Remaining Guesses: ");
        remainingGuesses.setFont(new Font("Arial Black", 16));
        //now add timer label
        timer = new Label("00:00:00");
        timer.setFont(new Font("Arial Black", 16));
        Label elapsedTime = new Label("Elapsed Time: ");
        elapsedTime.setFont(new Font("Arial Black", 16));
        //add game timer
        gameTimer = new GameTimer();
        gameTimer.startTimer(timer);

        gameWorkspace.getTimerBox().getChildren().addAll(elapsedTime, timer);
        remainingGuessBox.getChildren().addAll(remainingGuesses, remains);
        initWordGraphics(guessedLetters);
        showHint();
        showInfo();
        shapes = gameWorkspace.getShapes();
        play();
    }

    //render all letters in a grid pane
    private void showGridLetters(GridPane pane){
        String[] aToz = {"Q","W","E","R","T","Y","U","I","O","P",
                         "A","S","D","F","G","H","J","K","L",
                         "Z","X","C","V","B","N","M"};
        Text[] letters = new Text[aToz.length];
        for(int i = 0; i < aToz.length; i++){
            letters[i] = new Text(aToz[i]);
            letters[i].setFont(Font.font("Arial Black", 23));
        }
        letterBox = new WordBox(letters, 37);
        letterBox.setBox(Color.LIMEGREEN, Color.WHITE, 1.5);
        pane.setHgap(0.5);
        pane.setVgap(0.5);
        for(int i = 0; i < letterBox.size(); i++) {
            setLetterBoxOnMouseInteraction(letterBox,i);
            if(i<10)          pane.add(letterBox.get(i), i, 0);
            if(10<=i && i<19) pane.add(letterBox.get(i), i-10, 1);
            if(19<=i)         pane.add(letterBox.get(i), i-19, 2);
        }
    }

    private void end() {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        disableAllLetterBoxesMouseInteraction();
        gameButton.setDisable(true);
        gameInfo.setDisable(true);
        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            //String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            String endMessage = "";
            if (!success) {
                for(int i = 0; i < progress.length; i++){
                    if(!progress[i].isVisible()){
                        progress[i].setVisible(true);
                        progress[i].setFill(Color.RED);
                        boxes.setBoxFillColor(i, Color.DARKGRAY);
                    }
                }
                gameWorkspace.renderHangmanEndGraphics(false);
                endMessage = "Game Over\n\n" + "Play Time ：" + gameTimer.getTimeInFormat();
            }
            else {
                gameWorkspace.renderHangmanEndGraphics(true);
                endMessage = "Congratulations, You Won!\n\n" + "Play Time ：" + gameTimer.getTimeInFormat();
            }
            if (dialog.isShowing())
                dialog.toFront();
            else
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });
    }

    //show hint button
    private void showHint(){
        if(gamedata.getUniqueChar() > GameData.TOTAL_NUMBER_OF_UNIQUE_LETTERS_TO_SHOW_HINT ){
            enableHintButton();
            gameHint.setVisible(true);
            gameHint.setOnMouseClicked(e -> handleHintRequest());
        }
    }
    //show info button
    private  void showInfo(){
        enableInfoButton();
        gameInfo.setVisible(true);
        gameInfo.setOnMouseClicked(e -> handleInfoRequest());
    }
    //render word graphics
    private void initWordGraphics(HBox guessedLetters) {
        char[] tarWord = gamedata.getTargetWord().toCharArray();
        progress = new Text[tarWord.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(tarWord[i]));
            progress[i].setVisible(false);
            progress[i].setFont(Font.font ("Verdana", 20));
            progress[i].setFill(Color.WHITE);
        }
        boxes = new WordBox(progress,25);
        boxes.setBoxFillColor(Color.BLACK);
        guessedLetters.getChildren().addAll(boxes);
    }

    //dynamically render hangman graphics
    //according the number of remaining guesses, render the corresponding graphics
    private void renderHangmanGraphics(int remainingGuesses) {
        if (remainingGuesses >= 0 && remainingGuesses <= 9)
            shapes.get(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED - remainingGuesses - 1).setVisible(true);
    }

    //for input from both key press and mouse click, it is wise to have a method to process input guess
    private void processGuess(char guess){
            if (!alreadyGuessed(guess)) {
                boolean goodguess = false;
                for (int i = 0; i < progress.length; i++) {
                    if (gamedata.getTargetWord().charAt(i) == guess) {
                        progress[i].setVisible(true);
                        gamedata.addGoodGuess(guess);
                        goodguess = true;
                        discovered++;
                    }
                }
                if (!goodguess) {
                    gamedata.addBadGuess(guess);
                    renderTriedLetters(letterBox, guess);
                }
                else {
                    renderTriedLetters(letterBox, guess);
                }
                success = (discovered == progress.length);
                remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                //render hangman graphics according to remaining guesses
                renderHangmanGraphics(gamedata.getRemainingGuesses());
            }
            setGameState(GameState.INITIALIZED_MODIFIED);
    }

    public void play() {
        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    //check input
                    String key = event.getCharacter().toLowerCase();
                    if (!key.matches("[a-z]+")){
                            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
                            String endMessage ="Invalid key! \n\nPlease enter an alphabetical key (A-Z)";
                                dialog.show("Input Error", endMessage);
                    }
                    //valid input, and process input guess
                    else
                        processGuess(key.charAt(0));
                });
                //disable hint button if player can win by clicking hint
                if (gamedata.getUniqueChar() - gamedata.getGoodGuesses().size() == 1 ) {
                    disableHintButton();
                }
                //disable hint button if the remaining guess is 1, losing by clicking hint is pointless
                if(gamedata.getRemainingGuesses() == 1){
                    disableHintButton();
                }
                if (gamedata.getRemainingGuesses() <= 0 || success) {
                    disableHintButton();
                    gameTimer.stopTimer();
                    stop();
                }
                //if game state is uninitialized, thus a new game, disable input
                if(gamestate.equals(GameState.UNINITIALIZED)){
                    appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
                }
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    //render guessed letters
    private void renderTriedLetters(WordBox letterBox, Character c){
        for(int i = 0;i < letterBox.size(); i++){
            //find the guessed letter's position, then change its background color
            Text letter = (Text)letterBox.get(i).getChildren().get(1);
            if(letter.getText().toLowerCase().equals(c.toString())){
                letterBox.setBoxFillColor(i, Color.DARKGREEN);
            }
        }
    }

    private void restoreGUI() {
        boolean hintUsed = false;
        disableGameButton();
        enableInfoButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();
        shapes = gameWorkspace.getShapes();

        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);

        //show hangman pane
        HBox.setHgrow(gameWorkspace.getFigurePane(), Priority.ALWAYS);
        //make GameTextsPane look nicer
        gameWorkspace.getGameTextsPane().setSpacing(2);
        gameWorkspace.getGameTextsPane().setPadding(new Insets(2,2,2,2));
        //make guessed letter pane look nicer
        guessedLetters.setSpacing(1);
        //make tried letter pane look nicer
        showGridLetters(gameWorkspace.getTriedLetters());

        restoreWordGraphics(guessedLetters);
        restoreTriedLettersGraphics(letterBox);
        restoreHangmanGraphics(gamedata.getRemainingGuesses());

        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remains.setFont(new Font("Arial Black", 16));
        Label remainingGuesses = new Label("Remaining Guesses: ");
        remainingGuesses.setFont(new Font("Arial Black", 16));
        remainingGuessBox.getChildren().addAll(remainingGuesses, remains);
        //restore timer label
        timer = new Label("00:00:00");
        timer.setFont(new Font("Arial Black", 16));
        Label elapsedTime = new Label("Elapsed Time: ");
        elapsedTime.setFont(new Font("Arial Black", 16));
        //restore timer
        gameTimer = new GameTimer();
        gameTimer.convertSecToHourMinSec(gamedata.getTime());
        gameTimer.formatTime(timer);
        gameTimer.startTimer(timer);
        gameWorkspace.getTimerBox().getChildren().addAll(elapsedTime, timer);

        success = false;
        //recount unique char,since it is not saved to json file
        gamedata.countUniqueChar(gamedata.getTargetWord());
        //tangle hint button
        if(gamedata.getUniqueChar() > GameData.TOTAL_NUMBER_OF_UNIQUE_LETTERS_TO_SHOW_HINT ) {

            //check if goodGuesses and badGuesses have same element, if so it means hint has been used
            for(Character c: gamedata.getGoodGuesses()){
                if(gamedata.getBadGuesses().contains(c)){
                    //disable hint button, BUT show hint button, so players can know they have used hint already
                    disableHintButton();
                    gameHint.setVisible(true);
                    hintUsed = true;
                }
            }
            if(!hintUsed) {
                enableHintButton();
                gameHint.setVisible(true);
                gameHint.setOnMouseClicked(e -> handleHintRequest());
            }
        }
        play();
    }

    //re-render guessed letters
    private void restoreTriedLettersGraphics(WordBox letterBox){
        for(Character c: gamedata.getBadGuesses()){
            renderTriedLetters(letterBox, c);
        }
        for(Character c: gamedata.getGoodGuesses()){
            renderTriedLetters(letterBox, c);
        }
    }

    private void restoreWordGraphics(HBox guessedLetters) {
        discovered = 0;
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            progress[i].setFont(Font.font ("Verdana", 20));
            progress[i].setFill(Color.WHITE);
            if (progress[i].isVisible())
                discovered++;
        }
        boxes = new WordBox(progress,25);
        boxes.setBoxFillColor(Color.BLACK);
        guessedLetters.getChildren().addAll(boxes);
    }

    private void restoreHangmanGraphics(int remainingGuess){
        for (int i = GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED; i >= remainingGuess; i--) {
            //for example: if saved game remaining guesses = 7, meaning the player made 3 bad guesses
            //then loading should renderHangmanGraphics(10 to 7)
            //thus renderHangmanGraphics(i), as i decrements from 10 to 7
            renderHangmanGraphics(i);
        }
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
            disableHintButton();
            enableInfoButton();
            gameInfo.setVisible(false);
            gameHint.setVisible(false);
            //reset timer
            if(gameTimer != null) {
                gameTimer.stopTimer();
                gameTimer.resetTimer();
            }
            //every time player makes a new game, make sure game state is uninitialized BEFORE game starts
            setGameState(GameState.UNINITIALIZED);
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }
    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            if(gameTimer.isRunning())
                gameTimer.pauseTimer();
            //update time in game data
            updateGameDataTime();

            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null) {
                save(selectedFile.toPath());
                gameTimer.resumeTimer();
            }
            else
                gameTimer.resumeTimer();
        } else {
            if(gameTimer.isRunning()) {
                gameTimer.pauseTimer();
                //update time in game data
                updateGameDataTime();
            }
            save(workFile);
            gameTimer.resumeTimer();
        }
    }

    @Override
    public void handleLoadRequest() throws IOException {
        if(gameTimer != null)
            gameTimer.pauseTimer();
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists()) {
                load(selectedFile.toPath());
                restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
            }
            //no file selected, resume game
            if(!gamestate.equals(GameState.ENDED) && !gamestate.equals(GameState.UNINITIALIZED)) {
                if (selectedFile == null)
                    gameTimer.resumeTimer();
            }
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    //method that handles hint request, upon clicking hint button
    private void handleHintRequest() {
        Random rand = new Random();
        boolean validPick = false;
        char hint;
        int randNum;
        while (!validPick) {
            randNum = rand.nextInt(progress.length);
            if (!progress[randNum].isVisible()) {
                validPick = true;
                hint = (progress[randNum].getText()).charAt(0);
                for (int i = 0; i < progress.length; i++) {
                    if (gamedata.getTargetWord().charAt(i) == hint) {
                        progress[i].setVisible(true);
                        discovered++;
                    }
                }
                    //if add hint to goodGuesses, word will miss the hint when loading
                    //if add hint to badGuesses, remaining guesses will increase by 1 when loading
                    //solution: add hint to both guessed set, and handle the redundant letter when loading
                    gamedata.addBadGuess(hint);
                    gamedata.addGoodGuess(hint);

                    remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                    renderHangmanGraphics(gamedata.getRemainingGuesses());
                    //render guessed letters
                    renderTriedLetters(letterBox, hint);
                    //disable hint
                    disableHintButton();
                    setGameState(GameState.INITIALIZED_MODIFIED);
                    play();
                }
            }
        }
    //method that handles info request, upon clicking info button
    //note that currently it reveals the target word if the user clicks ok
    private void handleInfoRequest(){
        gameTimer.pauseTimer();
        ButtonType ok = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION,
                "Do you wish to reveal the target word?", ok, cancel);
        dialog.setHeaderText("Target Word Reveal");
        dialog.setTitle("Info");
        Optional<ButtonType> response = dialog.showAndWait();
        if(response.isPresent() && response.get() == ok) revealTargetWord();
        if(response.isPresent() && response.get() == cancel) gameTimer.resumeTimer();
    }
    private void revealTargetWord(){
        AppMessageDialogSingleton dialog   = AppMessageDialogSingleton.getSingleton();
        dialog.show("Info", "Target Word Revealed: \n\n" + gamedata.getTargetWord());
        if(!dialog.isShowing() && !gameTimer.isRunning())
            gameTimer.resumeTimer();
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        if(gameTimer.isRunning())
            gameTimer.pauseTimer();
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();
        if(yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL))
            gameTimer.resumeTimer();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }

    //helper method that update game data time only upon clicking save button
    private void updateGameDataTime() {
        if(!gameTimer.isRunning()){
            gamedata.setTime(gameTimer.getTimeInSec());
        }
    }

    //set a letterbox on mouse interaction
    private void setLetterBoxOnMouseInteraction(ArrayList<StackPane> list, int index){
        StackPane pane = list.get(index);
        Rectangle box = (Rectangle) pane.getChildren().get(0);
        Paint color = box.getStroke();
        pane.setOnMouseEntered(e -> box.setStroke(Color.DARKBLUE));
        pane.setOnMouseExited(e -> box.setStroke(color));
        pane.setOnMouseClicked(e -> handleMouseClickRequest(list, index));
    }
    //disable all mouse interactions
    private void disableAllLetterBoxesMouseInteraction(){
        if(!letterBox.isEmpty()){
            for(StackPane pane: letterBox){
                Rectangle box = (Rectangle) pane.getChildren().get(0);
                box.setStroke(Color.WHITE);
                //disable all mouse interactions
                pane.setOnMouseEntered(null);
                pane.setOnMouseExited(null);
                pane.setOnMouseClicked(null);
            }
        }
    }
    private void handleMouseClickRequest(ArrayList<StackPane> list, int index){
        StackPane pane = list.get(index);
        Text t = (Text) pane.getChildren().get(1);
        char guess = t.getText().toLowerCase().charAt(0);
        if(!alreadyGuessed(guess))
            processGuess(guess);
    }

    //inner class that serves as the graphical shell of set of letters
    //order of item adding onto StackPane: background -> letter(Text)
    class WordBox extends ArrayList<StackPane> {
        WordBox(Text[] progress, double boxSize) {
            for (Text text : progress) {
                Rectangle box = new Rectangle(boxSize, boxSize);
                box.setFill(Color.TRANSPARENT);
                box.setStroke(Color.BURLYWOOD);
                box.setStrokeWidth(2);
                this.add(new StackPane(box, text));
            }
        }
        //can change properties of box
        void setBox(Color fillColor, Color outlineColor, double outLineWidth){
            for(StackPane pane: this) {
                Rectangle box = (Rectangle) pane.getChildren().get(0);
                box.setFill(fillColor);
                box.setStroke(outlineColor);
                box.setStrokeWidth(outLineWidth);
            }
        }
        //set background color of all boxes
        void setBoxFillColor(Color fillColor){
            for(StackPane pane: this) {
                Rectangle box = (Rectangle) pane.getChildren().get(0);
                box.setFill(fillColor);
            }
        }
        //overloaded method, set background color of the box at the specific index
        void setBoxFillColor(int index, Color fillColor){
            if(index >= 0 && index<this.size()){
                Rectangle box = (Rectangle) this.get(index).getChildren().get(0);
                box.setFill(fillColor);
            }
        }
    }
}

