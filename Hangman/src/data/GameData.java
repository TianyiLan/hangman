package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import controller.GameError;
import ui.AppMessageDialogSingleton;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 * @author Ritwik Banerjee
 * @author Tianyi Lan
 */
public class GameData implements AppDataComponent {

    public static final  int TOTAL_NUMBER_OF_GUESSES_ALLOWED = 10;
    public static final  int TOTAL_NUMBER_OF_UNIQUE_LETTERS_TO_SHOW_HINT = 7;
    private static final int TOTAL_NUMBER_OF_STORED_WORDS    = 330622;

    private String         targetWord;
    private int            uniqueChar;      //store the number of unique character of the target word
    private Set<Character> goodGuesses;
    private Set<Character> badGuesses;
    private int            remainingGuesses;
    public  AppTemplate    appTemplate;
    private int            time;           //store elapsed time of the game

    public GameData(AppTemplate appTemplate) {
        this(appTemplate, false);
    }

    public GameData(AppTemplate appTemplate, boolean initiateGame) {
        if (initiateGame) {
            this.appTemplate = appTemplate;
            this.targetWord = setTargetWord();
            this.goodGuesses = new HashSet<>();
            this.badGuesses = new HashSet<>();
            this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
            this.time = 0;
        } else {
            this.appTemplate = appTemplate;
        }
    }

    public void init() {
        this.targetWord = setTargetWord();
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
    }

    @Override
    public void reset() {
        this.targetWord = null;
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }

    public String getTargetWord() {
        return targetWord;
    }
    public int getUniqueChar() {return uniqueChar;}
    public int getTime() {return time;}
    public void setTime(int time){this.time = time;}

    private String setTargetWord() {
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;
        String testTargetString = "";
        int toSkip;
        String invalidWords = "";
        boolean wordFound = false;
        while (!wordFound) {
            try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
                toSkip = new Random().nextInt(TOTAL_NUMBER_OF_STORED_WORDS);
                testTargetString = lines.skip(toSkip).findFirst().get();
                testTargetString = testTargetString.toLowerCase();
                //test isAlphabetical, if true then we have a valid target word
                if (isAlphabetical(testTargetString)) {
                    //count unique char
                    countUniqueChar(testTargetString);
                    //end the loop
                    wordFound = true;
                    //if we have any invalid words, show them to the user
                    if(!invalidWords.equals("")){
                        AppMessageDialogSingleton dialog   = AppMessageDialogSingleton.getSingleton();
                        dialog.show("Target Word Filter", "Filtered Invalid Words: \n\n" + invalidWords);
                    }
                }
                //if false then we pick a non-alphabetical word, put it in invalidWords
                else {
                    invalidWords += testTargetString + "\n";
                }
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
                System.exit(1);
                throw new GameError("Unable to load initial target word.");
            }
        }
        return testTargetString;
    }

    //reject non alphabetical words from being the target word
    private boolean isAlphabetical(String str){
        //check string against regex [a-z]+, note that [a-z] only checks strings with length 1
        return str.matches("[a-z]+");
    }

    //count unique char of a given string
    public void countUniqueChar(String str){
        ArrayList<Character> unique = new ArrayList<>();
        for(int i = 0; i < str.length(); i++) {
            //if unique does not contain such char
            if (!unique.contains(str.charAt(i)))
                //then add such char to unique
                unique.add(str.charAt(i));
        }
        uniqueChar = unique.size();
    }

    public GameData setTargetWord(String targetWord) {
        this.targetWord = targetWord;
        return this;
    }

    public Set<Character> getGoodGuesses() {
        return goodGuesses;
    }

    public GameData setGoodGuesses(Set<Character> goodGuesses) {
        this.goodGuesses = goodGuesses;
        return this;
    }

    public Set<Character> getBadGuesses() {
        return badGuesses;
    }

    public GameData setBadGuesses(Set<Character> badGuesses) {
        this.badGuesses = badGuesses;
        return this;
    }

    public int getRemainingGuesses() {
        return remainingGuesses;
    }

    public void addGoodGuess(char c) {goodGuesses.add(c);}

    public void addBadGuess(char c) {
        if (!badGuesses.contains(c)) {
            badGuesses.add(c);
            remainingGuesses--;
        }
    }
}