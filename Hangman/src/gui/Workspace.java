package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.animation.AnimationTimer;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

import static hangman.HangmanProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 * @author Tianyi Lan
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // container to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    Pane              figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              timerBox;          // text area displaying the game timer
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    GridPane          triedLetters;      // container to display the letters tried by player
    Button            startGame;         // the button to start playing a game of Hangman
    Button            hint;              // the button to give a hint of playing a game of Hangman
    Button            info;              // the button NEEDS TO BE DEVELOPED, currently it shows the target word for testing purpose
    ArrayList<Node>   shapes;            // data structure that stores all hangman shapes
    ReentrantLock     lock;              // lock for hangman animation
    Group             stickFigure;       // group of hangman graphics that animates upon winning
    HangmanController controller;

    double            xOffset                  = 140;  //for some reason, figurePane is not where it should be, so fix this adding offset
    double            yOffset                  = 100;
    double            hangmanApproximateWidth  = 60;
    double            hangmanApproximateHeight = 190;
    double            velocity                 = 2.0;
    double            xVelocity;
    double            yVelocity;
    double            maxRotation              = 20;
    double            rotationInc              = 0.1;
    double            rotation                 = 0;
    Rotate            rotate                   = new Rotate();
    Point2D localToParent;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
        lock = new ReentrantLock();
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        figurePane = new Pane();

        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        triedLetters = new GridPane();
        triedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        //timer pane
        timerBox = new HBox();

        gameTextsPane.getChildren().addAll(remainingGuessBox, guessedLetters, triedLetters, timerBox);

        bodyPane = new HBox();
        bodyPane.getChildren().addAll(figurePane, gameTextsPane);

        startGame = new Button("Start Playing");
        startGame.setTooltip(new Tooltip("Start playing the Hangman game"));
        hint = new Button("Hint");
        hint.setTooltip(new Tooltip("Reveal an unguessed letter"));
        info = new Button("?");
        info.setTooltip(new Tooltip("Show the target word"));

        hint.setVisible(false);
        info.setVisible(false);
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight, info);

        workspace = new VBox();
        workspace.getChildren().addAll(headPane, bodyPane, footToolbar);
        VBox.setVgrow(bodyPane,Priority.ALWAYS);
    }

    //render all hangman graphics,but make them not visible
    private void layoutHangmanGraphics() {
        shapes = new ArrayList<>();
        String gallowsColor = "#5c3c32";
        double gallowsWidth = 10;
        double hangmanWidth = 5;
        Rectangle gallowsLowerHorizontal;
        Rectangle gallowsLowerVertical;
        Rectangle gallowsUpperHorizontal;
        Line gallowsUpperVertical;
        Group headGroup;           //render head, eyes and mouth together
        Circle head;
        Circle leftEye;
        Circle rightEye;
        Rectangle mouth;
        Rectangle torso;
        Rectangle leftArm;
        Rectangle rightArm;
        Rectangle leftLeg;
        Rectangle rightLeg;
        if (shapes.isEmpty()) {
            //render all graphics BUT MAKE THEM INVISIBLE
            double baseX = figurePane.getLayoutX() + 20;
            double baseY = figurePane.getLayoutY() + 360;
            //first gallows
            gallowsLowerHorizontal = new Rectangle(baseX, baseY, 250, gallowsWidth);
            gallowsLowerHorizontal.setFill(Paint.valueOf(gallowsColor));
            gallowsLowerVertical = new Rectangle(baseX, baseY - 350, gallowsWidth, 350);
            gallowsLowerVertical.setFill(Paint.valueOf(gallowsColor));
            gallowsUpperHorizontal = new Rectangle(baseX, baseY - 350, 158.5, gallowsWidth);
            gallowsUpperHorizontal.setFill(Paint.valueOf(gallowsColor));
            gallowsUpperVertical = new Line(baseX + 156, baseY - 344, baseX + 156, baseY - 288);
            gallowsUpperVertical.setStrokeWidth(hangmanWidth);
            gallowsUpperVertical.setStroke(Paint.valueOf(gallowsColor));
            gallowsUpperVertical.setFill(Paint.valueOf(gallowsColor));
            //then hangman
            headGroup = new Group();
            head = new Circle(baseX + 155, baseY - 258, 30);
            head.setFill(Color.TRANSPARENT);
            head.setStrokeWidth(hangmanWidth);
            head.setStroke(Color.BLACK);
            leftEye = new Circle(baseX + 145, baseY - 268, 4);
            leftEye.setFill(Color.BLUE);
            rightEye = new Circle(baseX + 165, baseY - 268, 4);
            rightEye.setFill(Color.BLUE);
            mouth = new Rectangle(baseX + 140, baseY - 250, 30, 6);
            mouth.setFill(Color.ORANGE);
            headGroup.getChildren().addAll(head,leftEye,rightEye,mouth);     //mouth index 3
            torso = new Rectangle(baseX + 153, baseY - 228, hangmanWidth, 70);
            leftArm = new Rectangle(baseX + 134, baseY - 219, hangmanWidth, 50);
            leftArm.setRotate(45);
            rightArm = new Rectangle(baseX + 172, baseY - 219, hangmanWidth, 50);
            rightArm.setRotate(135);
            leftLeg = new Rectangle(baseX + 129, baseY - 170, hangmanWidth, 65);
            leftLeg.setRotate(45);
            rightLeg = new Rectangle(baseX + 177, baseY - 170, hangmanWidth, 65);
            rightLeg.setRotate(135);
            shapes.add(gallowsLowerHorizontal);   //remaining guesses = 9
            shapes.add(gallowsLowerVertical);     //8
            shapes.add(gallowsUpperHorizontal);   //7
            shapes.add(gallowsUpperVertical);     //6
            shapes.add(headGroup);                //5 (array index 4)
            shapes.add(torso);                    //4
            shapes.add(leftArm);                  //3
            shapes.add(rightArm);                 //2
            shapes.add(leftLeg);                  //1
            shapes.add(rightLeg);                 //0
            //add them to pane,and make them invisible
            for (Node e : shapes) {
                figurePane.getChildren().add(e);
                e.setVisible(false);
            }
        }
    }

    //based on the result of the game, the hangman either smiles on winning, or grieves on lose
    public void renderHangmanEndGraphics(boolean alive){
        Group face = (Group) shapes.get(4);
        double mouthX = figurePane.getLayoutX() + 175;
        double mouthY = figurePane.getLayoutY() + 107;
        //alive, then smile
        if(alive){
            //get a random angle in radians at which the hangman moves
            double randomRadians = Math.PI * 2 * Math.random();
            xVelocity = Math.sin(randomRadians) * velocity;
            yVelocity = Math.cos(randomRadians) * velocity;

            stickFigure = new Group();
            figurePane.getChildren().clear();
            Arc smile = new Arc(mouthX, mouthY, 15.5, 16, 180, 180);
            smile.setType(ArcType.CHORD);
            smile.setStroke(Color.ORANGE);
            smile.setStrokeWidth(6);
            smile.setFill(Color.TRANSPARENT);
            face.getChildren().set(3, smile);
            shapes.set(4,face);
            for (int i= 4; i<shapes.size();i++) {
                shapes.get(i).setVisible(true);
                stickFigure.getChildren().add(shapes.get(i));
            }
            figurePane.getChildren().add(stickFigure);
            //add a text
            Text aliveText= new Text("YOU SAVED HANGMAN!");
            aliveText.setFont(Font.font("Comic Sans MS", 30));
            aliveText.setFill(Color.RED);
            figurePane.getChildren().add(aliveText);
            aliveText.setTranslateX(10);
            aliveText.setTranslateY(380);
            //create animation timer
            HangmanAnimationTimer hangmanAnimationTimer = new HangmanAnimationTimer(alive);
            //start animation
            hangmanAnimationTimer.start();
        }
        //dead, then grieve
        else {
            Arc grieve = new Arc(mouthX, mouthY+10, 15.5, 8, 30, 122);
            grieve.setType(ArcType.OPEN);
            grieve.setStroke(Color.ORANGE);
            grieve.setStrokeWidth(6);
            grieve.setFill(Color.TRANSPARENT);
            face.getChildren().set(3, grieve);
            shapes.set(4,face);
            stickFigure = new Group();
            //add lost hangman figure to stick figure group
            for (int i= 4; i<shapes.size();i++) {
                stickFigure.getChildren().add(shapes.get(i));
            }
            figurePane.getChildren().add(stickFigure);
            //swing stick
            Line swingStick = (Line)shapes.get(3);
            rotate.setPivotX(figurePane.getLayoutX()+172);
            rotate.setPivotY(figurePane.getLayoutY()+12);
            swingStick.getTransforms().add(rotate);
            //add a text
            Text deadText= new Text("HANGMAN IS DEAD!");
            deadText.setFont(Font.font("Comic Sans MS", 30));
            deadText.setFill(Color.RED);
            figurePane.getChildren().add(deadText);
            deadText.setTranslateX(40);
            deadText.setTranslateY(350);
            //create animation timer
            HangmanAnimationTimer hangmanAnimationTimer = new HangmanAnimationTimer(alive);
            //start animation
            hangmanAnimationTimer.start();
        }
    }
    private void moveHangman(Group stickFigure, double percentage, boolean alive){
        //if hangman is alive
        if(alive) {
            //move hangman according to its x and y velocity
            stickFigure.translateXProperty().setValue(stickFigure.getTranslateX() + (xVelocity * percentage));
            stickFigure.translateYProperty().setValue(stickFigure.getTranslateY() + (yVelocity * percentage));
            stickFigure.setRotate(rotation);
            rotation += rotationInc;
            //if hangman touches the left side boundary of its container
            if (((xVelocity < 0) && ((stickFigure.translateXProperty().getValue()) < (0 - xOffset)))
                    ||
                    //if hangman touches the left side boundary of its container
                    ((xVelocity > 0) && ((stickFigure.translateXProperty().getValue() + hangmanApproximateWidth) > (figurePane.getWidth() - xOffset)))) {
                //change xVelocity's direction
                xVelocity *= -1;
            }
            //if hangman touches the top side boundary of its container
            if (((yVelocity < 0) && ((stickFigure.translateYProperty().getValue()) < (0 - yOffset)))
                    ||
                    //if hangman touches the bottom side boundary of its container
                    ((yVelocity > 0) && ((stickFigure.translateYProperty().getValue() + hangmanApproximateHeight) > (figurePane.getHeight() - yOffset)))) {
                //change yVelocity's direction
                yVelocity *= -1;
            }
            //make hangman swing
            if (rotation > maxRotation) rotationInc *= -1;
            if (rotation < -maxRotation) rotationInc *= -1;
        }
        //if hangman is dead
        else{
            rotate.setAngle(rotation);
            rotation += rotationInc;
            //find the coordinates of the end point of the swing stick  AFTER transformation
            localToParent = shapes.get(3).localToParent(((Line) shapes.get(3)).getEndX(), ((Line) shapes.get(3)).getEndY());
            //and move stick figure
            //NOTE that, the X,Y of stickFigure is (0,0), thus need to subtract (176,72), the end point of line, to move stick figure back
            stickFigure.translateXProperty().setValue(localToParent.getX() - 176);
            stickFigure.translateYProperty().setValue(localToParent.getY()- 72);
            //make stick swing
            if (rotation > maxRotation) rotationInc *= -1;
            if (rotation < -maxRotation) rotationInc *= -1;
        }
    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);

        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));
    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }
    public HBox getTimerBox() {return timerBox;}
    public GridPane getTriedLetters() {return triedLetters;}
    public Pane getFigurePane() {return figurePane;}
    public ArrayList<Node> getShapes(){return shapes;}

    public Button getStartGame() {return startGame;}
    public Button getHint() {return hint;}
    public Button getInfo() {return info;}

    public void reinitialize() {
        timerBox = new HBox();
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        triedLetters = new GridPane();
        triedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, triedLetters, timerBox);
        //add hint to grid pane, below all letter-gird
        triedLetters.add(hint, 0, 3);

        bodyPane.getChildren().setAll(figurePane, gameTextsPane);
        figurePane.getChildren().clear();
        layoutHangmanGraphics();
    }

    //inner class that runs the hangman animation
    class HangmanAnimationTimer extends AnimationTimer {
        long lastTime = -1;
        long targetTimePerFrame = 20000000;
        boolean alive;
        HangmanAnimationTimer(boolean alive){
            //call constructor from its super class
            super();
            //set value for alive
            this.alive = alive;
        }

        @Override
        public void handle(long now) {
            try {
                //work begin, lock
                Workspace.this.lock.lock();

                //update last time
                if (lastTime < 0)
                    lastTime = now - targetTimePerFrame;
                long timeDiff = now - lastTime;
                lastTime = now;
                double percentage = ((double) timeDiff) / targetTimePerFrame;

                //scale hangman movements according to the current frame speed
                moveHangman(stickFigure, percentage, alive);

                if(!controller.getGamestate().equals(HangmanController.GameState.ENDED)){
                    this.stop();
                }
            } finally {
                //work done, release lock
                Workspace.this.lock.unlock();
            }
        }
    }
}

